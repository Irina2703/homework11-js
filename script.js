"use strict"

/* 
Теоретичні питання
1. Що таке події в JavaScript і для чого вони використовуються?

   Подія у JavaScript це сигнал від браузера що щось сталося. Щоб виконати дію (код) на потрібну подію необхідно для події вказати функцію яка буде виконуватися коли настане подія.
   Події можна призначити обробник, тобто функцію, яка спрацює, щойно подія сталася.
   Саме завдяки обробникам JavaScript код може реагувати на дії користувача.




2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.
   Події миші:

click – відбувається, коли клацнули на елемент лівою кнопкою миші (на пристроях із сенсорними екранами воно відбувається при торканні).
contextmenu – відбувається, коли клацнули на елемент правою кнопкою миші.
mouseover / mouseout – коли миша наводиться на / залишає елемент.
mousedown / mouseup – коли натиснули / відпустили кнопку миші на елементі.
mousemove – під час руху миші.

3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?
    Контекстне меню (ContextMenu) — це список команд, заснованих на контексті вибору, наведення курсору миші або фокусування на клавіатурі. Вони є одними з найефективніших і поширених робочих областей команд і можуть бути використані в різних місцях.

    Подія "contextmenu" відбувається, коли користувач викликає контекстне меню для елемента веб-сторінки. Це відбувається, наприклад, коли користувач праворуч клікає на елементі веб-сторінки або використовує альтернативний спосіб виклику контекстного меню на тач-пристроях.

    В JavaScript подія "contextmenu" може бути використана для відслідковування цього події і виконання певних дій. Наприклад, можна заборонити виклик контекстного меню за допомогою цієї події, використовуючи методи preventDefault() або return false, а також виконувати певні дії при виклику контекстного меню на конкретному елементі веб-сторінки.

    Отже, подія "contextmenu" відображає взаємодію користувача з контекстним меню на веб-сторінці і дозволяє виконати певні дії на цій події за допомогою JavaScript.

Практичні завдання
 1. Додати новий абзац по кліку на кнопку:
  По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">
 
 2. Додати новий елемент форми із атрибутами:
 Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
  По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.
 */


 
/*1*/

const buttonClick = document.querySelector("#btn-click")

function createNewP () {
    const newP = document.createElement("p");
    newP.innerText = "New Paragraph";
    const sectionContent = document.querySelector("#content");
    sectionContent.append(newP);
};


buttonClick.addEventListener('click', () => {
    createNewP();
})

/* 2 
 Додати новий елемент форми із атрибутами:
 Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
  По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.*/


const newElement = document.createElement("button");
newElement.id = "btn-input-create";
// console.log(newElement);

const sectionContent = document.querySelector("#content");
sectionContent.append(newElement);


function createNewInput() {
    const newInput = document.createElement("input")
    console.log(newInput);
    newInput.setAttribute('name', 'New Input');
    newInput.setAttribute('type', 'text');
    newInput.setAttribute('placeholder', 'enter the text');
    newElement.after(newInput);
}
newElement.addEventListener('click', () => {
    createNewInput();
})